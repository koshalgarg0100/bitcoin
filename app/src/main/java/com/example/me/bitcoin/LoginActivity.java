package com.example.me.bitcoin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaCodec;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuAdapter;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvingResultCallbacks;
import com.google.android.gms.common.api.Status;

public class LoginActivity extends AppCompatActivity {
    public static GoogleApiClient mGoogleApiClient;
    private TextView mStatusTextView;
    private ProgressDialog mProgressDialog;

    public  static SharedPreferences sp;
    public  static  SharedPreferences.Editor edit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sp=getSharedPreferences("bitcoin",MODE_PRIVATE);
        edit=sp.edit();

        if(sp.getString("login","false").equals("true"))
        {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }


        // Configure sign-in to request the user's ID, email address, and basic profile. ID and
// basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

// Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
               // .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        SignInButton signInButton = (SignInButton) findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        findViewById(R.id.sign_in_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, 0);
    }

    private  void signout()
    {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResolvingResultCallbacks<Status>(LoginActivity.this,0) {
            @Override
            public void onSuccess(@NonNull Status status) {
                //LoginActivity.edit.putString("login","false");
                Toast.makeText(LoginActivity.this, "logged out", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onUnresolvableFailure(@NonNull Status status) {
                Toast.makeText(LoginActivity.this, "Logout Failed", Toast.LENGTH_SHORT).show();


            }
        });
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == 0) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("log", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.i("loginfo",acct.getDisplayName()+acct.getPhotoUrl()+acct.getEmail());
            //Toast.makeText(this, acct.getDisplayName()+acct.getPhotoUrl()+acct.getEmail(), Toast.LENGTH_SHORT).show();

            edit.putString("name",acct.getDisplayName());
            edit.putString("email",acct.getEmail());
            edit.putString("pic", String.valueOf(acct.getPhotoUrl()));
            edit.putString("login","true");
            edit.commit();
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();

        } else {
            Toast.makeText(this, "Sign In failed,Try Again", Toast.LENGTH_SHORT).show();
        }
    }
}

package com.example.me.bitcoin;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuAdapter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.ResolvingResultCallbacks;
import com.google.android.gms.common.api.Status;

public class MainActivity extends AppCompatActivity {

    Button usd,inr,logout;
    TextView name,email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        usd= (Button) findViewById(R.id.btn_usd);
        inr= (Button) findViewById(R.id.btn_inr);
        logout= (Button) findViewById(R.id.logout);


        name= (TextView) findViewById(R.id.tv_name);
        email= (TextView) findViewById(R.id.tv_email);

        if(LoginActivity.sp.getString("login","false").equals("false"))
        {
            startActivity(new Intent(MainActivity.this,LoginActivity.class));
            finish();
        }
        else
        {
            name.setText(LoginActivity.sp.getString("name","false"));

            email.setText(LoginActivity.sp.getString("email","false"));

        }

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LoginActivity.edit.putString("login","false");
                LoginActivity.edit.commit();

                startActivity(new Intent(MainActivity.this,LoginActivity.class));
                        finish();
            }
        });


        usd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(MainActivity.this,GraphPlotActivity.class);
                i.putExtra("type","USD");
                startActivity(i);
            }
        });
        inr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i=new Intent(MainActivity.this,GraphPlotActivity.class);
                i.putExtra("type","INR");
                startActivity(i);
            }
        });


    }
}

package com.example.me.bitcoin;

import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.media.MediaCodec;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import  java.util.regex.Pattern;


public class GraphPlotActivity extends AppCompatActivity {

    String date;
    Double currenr;
    String type;
    Double one_usd_in_inr=1.0;
    TextView data;

    int days=8;
    String startDate;
    String curDate;

    Double dollar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_plot);

        Intent i=getIntent();
        type=i.getStringExtra("type");

        Log.i("log_type",type);
        Calendar c = Calendar.getInstance();
        long dateInMillis=c.getTimeInMillis();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        curDate = formatter.format(new Date(dateInMillis));

        Log.i("log_currenttime_millis",dateInMillis+"");
        dateInMillis-= days*86400000;
        Log.i("log_time_millis "+days+" ago",dateInMillis+"");

        startDate=formatter.format(new Date(dateInMillis));

        Log.i("log_dates",startDate+" "+curDate);

        if(type.equals("INR"))
        {
            fetchINR();
        }
        else
        fetchData();

        data= (TextView) findViewById(R.id.data);





    }

    private void fetchINR() {
        String url="https://www.getexchangerates.com/inr/";

        // String url="http://GOOGLE.COM";


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {

                    @Override

                    public void onResponse(String response) {


    Pattern p = Pattern.compile("</td><td>United States Dollar</td><td>(.*?)</td></tr>");
                        Matcher m = p.matcher(response);
                        if (m.find()){
                            String a=m.toMatchResult().group(0);
                            Log.i("log_regexxxxx",a);
                            String k=a.substring(38,46);
                            Log.i("log_regexxxxx",k);
                           dollar = Double.valueOf(k);
                            one_usd_in_inr=1/dollar;
                            Log.i("log_regexxxxx",one_usd_in_inr+"");
                            fetchData();

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Toast.makeText(GraphPlotActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };
        MySingleTon.getInstance(GraphPlotActivity.this).addtoRequestQueue(stringRequest);

            }

    private void fetchData() {
       String url="http://api.coindesk.com/v1/bpi/historical/close.json?start="+startDate+"&end="+curDate;

       // String url="http://GOOGLE.COM";


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {

                    @Override

                    public void onResponse(String response) {

                        Log.i("log_signup_response",response);
                        try {
                            JSONObject obj=new JSONObject(response);
                            JSONObject bpi=obj.getJSONObject("bpi");
                          //  Toast.makeText(GraphPlotActivity.this, bpi.length()+"", Toast.LENGTH_SHORT).show();
                            int len=bpi.length();
                            GraphView graph = (GraphView) findViewById(R.id.graph);
                            DataPoint points[]=new DataPoint[len];

                            Iterator iter = bpi.keys();
                            int i=0;

                            while(iter.hasNext()){

                                String key = (String)iter.next();
                                Double val=Double.parseDouble(bpi.getString(key));

                                if(type.equals("INR"))
                                    val*=one_usd_in_inr;

                                Log.i("log",key +" "+ i + " "+val+"");
                                DataPoint p=new DataPoint(i,val);
                                points[i]=p;
                                i++;
                                if(i==len)
                                {
                                    currenr=val;
                                    data.setText(startDate +" to "+curDate+"\n"+"CURRENT EXCHANGE PRICE \n in "+type+" \n  "+val);
                                   // Toast.makeText(GraphPlotActivity.this, ""+currenr, Toast.LENGTH_SHORT).show();
                                }
                            }
                            LineGraphSeries<DataPoint> series = new LineGraphSeries<>(points);
                            graph.addSeries(series);

                        } catch (JSONException e) {
                            Toast.makeText(GraphPlotActivity.this, "Invalid System Date", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {
                Toast.makeText(GraphPlotActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }

        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };
        MySingleTon.getInstance(GraphPlotActivity.this).addtoRequestQueue(stringRequest);
    }
}
